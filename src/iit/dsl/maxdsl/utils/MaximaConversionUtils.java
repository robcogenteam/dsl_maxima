package iit.dsl.maxdsl.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utilities to interact with a Maxima engine and retrieve the output
 * of the interpreter.
 */
public class MaximaConversionUtils
{
    /**
     * Interpret a matrix expression in Maxima language and retrieve the result.
     *
     * The expression is evaluated and possibly simplified by a Maxima
     * interpreter, and each element of the resulting matrix, in its final
     * form, is finally retrieved as a string (since Maxima is an end-user
     * application with text-based input and output).
     *
     * @param interpreter A wrapper of the Maxima runtime, which is assumed
     *   to know the variable referenced by the next parameter
     * @param maximaMatrix The name of the Maxima variable which will be
     *   evaluated during the exection of this function
     * @param rows Number of rows of the matrix
     * @param cols Number of columns
     *
     * @return A matrix of strings, each one being the textual output of
     *   Maxima, after evaluation and simplification of the Maxima expression.
     */
    public static String[][] getMatrix(MaximaRunner interpreter,
            String maximaMatrix, int rows, int cols)
    {
        String maximaOut;
        String[] tmpRow;
        String[][] retValue = new String[rows][cols];
        // Get the current line-length of the Maxima engine
        int line_length = Integer.parseInt(interpreter.getSingleLineOutput("linel;"));
        /*
         * Use 'ratsimp' to simplify things like '-(- ...)' which are not
         * compliant with the grammar of the language;'keepfloat' should be
         * enough to prevent 'ratsimp' from introducing further, unwanted
         * changes.. Use 'float' to make sure that the expressions that would
         * resolve into a float do so.
        */
        String matrix = "__mx";
        String expr = matrix + " : float(ratsimp("+ maximaMatrix +" )), keepfloat:true;";
        interpreter.run( expr );
        // Process the matrix element by element
        for(int r=0; r<rows; r++)
        {
            tmpRow = new String[cols];
            for(int c=0; c<cols; c++)
            {
                expr = "apply1(" + matrix + "[" +(r+1)+","+(c+1)+"] , mysqr );";
                maximaOut = interpreter.getSingleLineOutput( expr );
                tmpRow[c] = cleanMatrixRowOutput(maximaOut);

                if(tmpRow[c].length() > line_length) {
                    // A single element of the matrix does not fit the current line width
                    // This is very unlikely, unless linel is *really* small, or the
                    //  expression really long.
                    throw(new RuntimeException("The element (" + (r+1)+","+(c+1)+")" +
                            "of the matrix " + maximaMatrix + " is too long. " +
                            "Maybe increase the linel configuration value?"));
                }
            }
            retValue[r] = tmpRow;
        }
        return retValue;
    }

    static public String cleanMatrixRowOutput(String row) {
        return row.replaceAll("[\\[|\\]]", "")
        // Remove the useless '1.0*' inserted by Maxima because of 'float()' (?!?)
        // Match a preceding non-digit (\D) to avoid corrupting things like
        // '21.0*'... KEEP the matched non-digit in the substitution!
        .replaceAll("(\\D)1\\.0 *\\*", "$1")
        // Remove the ", which Maxima might insert when printing expressions
        //  which are really strings
        .replaceAll("\"", "")
        .trim();
    }

    // Regular expression to match a floating point number or an integer:
    static final String REGEXP_FLOAT =
            "^ *-? *[\\d]*(\\.[\\d]+(E-?[\\d]+)?)?$";
    static final Pattern p = Pattern.compile(REGEXP_FLOAT);
    @Deprecated
    public static boolean isConstant(String matrixElement) {
        Matcher m = p.matcher(matrixElement);
        return m.matches();
    }

    /**
     * Determines which elements of the given Maxima matrix are actual
     * numbers (i.e. not symbols).
     * @param interpreter The maxima engine to be used
     * @param matrix The name of a matrix variable that must be known by the
     *               given interpreter
     * @param rows Number of rows of the matrix
     * @param cols Number of columns of the matrix
     * @return A matrix of the same size of the Maxima matrix, whose elements
     * are set to true only if the element in the corresponding position in the
     * Maxima matrix is a number literal.
     */
    public static boolean[][] markFloatLiterals(
            MaximaRunner interpreter,
            String matrix,
            int rows, int cols)
    {
        return coeffWisePredicate(interpreter, matrix, rows, cols, "numberp");
    }
    /**
     *
     * @param interpreter
     * @param matrix
     * @param rows
     * @param cols
     * @return
     */
    public static boolean[][] markConstants(
            MaximaRunner interpreter,
            String matrix,
            int rows, int cols)
    {
        return coeffWisePredicate(interpreter, matrix, rows, cols, "constantp");
    }


    private static boolean[][] coeffWisePredicate(
            MaximaRunner interpreter,
            String matrix, int rows, int cols,
            String predicate)
    {
        boolean[][] ret = new boolean[rows][cols];
        String statement, output;
        for(int r=0; r<rows; r++) {
            for(int c=0; c<cols; c++) {
                statement = predicate + "(" + matrix + "["+(r+1)+","+(c+1)+"]" + ");" ;
                output = interpreter.getSingleLineOutput( statement );
                ret[r][c] = Boolean.parseBoolean(output);
            }
        }
        return ret;
    }

}
