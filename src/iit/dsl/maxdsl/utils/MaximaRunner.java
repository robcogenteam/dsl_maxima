package iit.dsl.maxdsl.utils;

import java.io.File;

import uk.ac.ed.ph.jacomax.JacomaxSimpleConfigurator;
import uk.ac.ed.ph.jacomax.MaximaConfiguration;
import uk.ac.ed.ph.jacomax.MaximaInteractiveProcess;
import uk.ac.ed.ph.jacomax.MaximaProcessLauncher;
import uk.ac.ed.ph.jacomax.MaximaTimeoutException;
import uk.ac.ed.ph.jacomax.utilities.MaximaOutputUtilities;

/**
 * Simple wrapper around Jacomax to execute Maxima and get the results as strings.
 * @author phd
 *
 */
public class MaximaRunner {
    public interface IConfigurator {
        public int getLinel();
        public int getFpprintprec();
    }

    public static class DefaultConfigurator implements IConfigurator {
        @Override public int getLinel()       {  return 4096;  }
        @Override public int getFpprintprec() {  return 6;  }
    }


    private MaximaConfiguration configuration = null;
    private MaximaProcessLauncher    launcher = null;
    private MaximaInteractiveProcess  process = null;

    public MaximaRunner() {
        this(new DefaultConfigurator());
    }

    public MaximaRunner(IConfigurator myCfg) {
        configuration = JacomaxSimpleConfigurator.configure();
        launcher = new MaximaProcessLauncher(configuration);
        process = launcher.launchInteractiveProcess();
        try {
            int linel       = myCfg.getLinel();
            int fpprintprec = myCfg.getFpprintprec();
            process.executeCall("stardisp:true;");    //displays a '*' for products
            process.executeCall("linel:"+linel+";");  //lines must be appropriately long, so Maxima does not split matrices
            process.executeCall("fpprintprec:"+fpprintprec+";"); //limits the number of printed digits for floats

            process.executeCall("display2d: false;");
            // Make sure the output is line-based, ie 1-dimensional strings
            // Beware that (bug??): display2d false screws up the 'linel' option, and ignores the 'stringdisp'

            // The following is a nice hack to replace squares with products,
            // as my grammar cannot currently parse "...^2", nor I contemplate
            // for square expressions. I define a simplification rule which I
            // then apply manually in getMatrix(). Note that 'f(x)' returns
            // a string, text, which cannot be further processed by Maxima
            // and which might contain unwanted '"' marks. Thanks to Richard
            // Fateman for the hint about this hack.
            process.executeCall("matchdeclare(x, all);");
            process.executeCall("f(x) := sconcat(x, sconcat(\"*\", x));");
            process.executeCall("defrule( mysqr, x^2, f(x) );");

        } catch (MaximaTimeoutException e) {
            throw(new RuntimeException("Maxima timeout exception"));
        }
    }

    public void runBatch(String scriptFullPath) {
        File libFile = new File(scriptFullPath);
        if( ! libFile.isFile() ) {
            throw new RuntimeException("Could not find this Maxima source file: " +
                    libFile.getPath() + "; is it the correct path?");
        }
        try {
            process.executeCall("batch(\"" + scriptFullPath + "\");");
        } catch (MaximaTimeoutException e) {
            throw(new RuntimeException("Maxima timeout exception"));
        }
    }

    public void run(String code) {
        try {
            process.executeCall(code);
        } catch (MaximaTimeoutException e) {
            throw(new RuntimeException("Maxima timeout exception"));
        }
    }

    public String getSingleLineOutput(String code) {
        String result;
        try {
            result = process.executeCall(code);
        } catch (MaximaTimeoutException e) {
            throw(new RuntimeException("Maxima timeout exception"));
        }
        return MaximaOutputUtilities.parseSingleLinearOutputResult(result);
    }

    public void terminate() {
        process.terminate();
    }
}

