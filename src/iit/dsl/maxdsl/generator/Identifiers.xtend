package iit.dsl.maxdsl.generator

import iit.dsl.maxdsl.maximaDsl.FloatLiteral
import iit.dsl.maxdsl.maximaDsl.Cosine
import iit.dsl.maxdsl.maximaDsl.Prod
import iit.dsl.maxdsl.maximaDsl.Sine
import iit.dsl.maxdsl.maximaDsl.Sum
import iit.dsl.maxdsl.maximaDsl.VarLiteral

/**
 * Misc utilities to turn expressions of this DSL into legal identifiers or
 * expressions for programming languages.
 */
class Identifiers
{
    static Identifiers instance = new Identifiers()
    def static Identifiers getInstance() {
        return instance
    }

    private new() {}

    def dispatch String toCode(Sum expr, IIdentifiersReplacement specs) {
        var lhs = expr.left .toCode(specs)
        var rhs = expr.right.toCode(specs)

        if(expr.left instanceof Prod) {
            lhs = "(" + lhs + ")"
        }
        if(expr.right instanceof Prod) {
            rhs = "(" + rhs + ")"
        }
        return lhs + expr.sign + rhs
    }

    def dispatch String toCode(Prod expr, IIdentifiersReplacement specs) {
        var lhs = expr.left .toCode(specs)
        var rhs = expr.right.toCode(specs)

        if(expr.left instanceof Sum) {
            lhs = "(" + lhs + ")"
        }
        if(expr.right instanceof Sum) {
            rhs = "(" + rhs + ")"
        }
        return lhs + " * " + rhs
    }

    def dispatch String toCode(FloatLiteral f, IIdentifiersReplacement specs) '''
        «IF f.minus»-«ENDIF» «f.value»'''

    def dispatch String toCode(VarLiteral v, IIdentifiersReplacement specs) '''
         «IF v.minus»-«ENDIF» «specs.valueExpression(v)»'''

    def dispatch String toCode(Sine expr, IIdentifiersReplacement specs) {
        var String minus = ""
        if(expr.minus) minus = "-"
        return (minus + specs.valueExpression(expr))
    }
    def dispatch String toCode(Cosine expr, IIdentifiersReplacement specs) {
        var String minus = ""
        if(expr.minus) minus = "-"
        return (minus + specs.valueExpression(expr))
    }
}