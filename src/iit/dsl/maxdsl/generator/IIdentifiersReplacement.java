package iit.dsl.maxdsl.generator;

import iit.dsl.maxdsl.maximaDsl.Cosine;
import iit.dsl.maxdsl.maximaDsl.Sine;
import iit.dsl.maxdsl.maximaDsl.VarLiteral;


/**
 * Interface for strategies that transform sines and cosines of
 * the MaximaDSL models into valid expressions of some programming
 * language.
 *
 * A specific implementation of this interface is required by a code
 * generator.
 *
 * @author Marco Frigerio
 */
public interface IIdentifiersReplacement
{
    /**
     * The expression that shall be used as the replacement of any occurrence
     * of the given variable.
     * @param argument The variable literal to be replaced
     * @return the string to be used to replace all the occurrences of the
     *         specified variable in a model
     */
     String valueExpression(VarLiteral var);
     /** @name Trigonometric functions
      * The replacement for cosine and sine expressions
      */
     ///@{
     /** @return the string to be used to replace occurrences of a sine */
     String valueExpression(Sine sine);
     /** @return the string to be used to replace occurrences of a cosine */
     String valueExpression(Cosine cosine);
     ///@}
}
