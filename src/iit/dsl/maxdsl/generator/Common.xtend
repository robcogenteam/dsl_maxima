package iit.dsl.maxdsl.generator

import iit.dsl.maxdsl.maximaDsl.Cosine
import iit.dsl.maxdsl.maximaDsl.FloatLiteral
import iit.dsl.maxdsl.maximaDsl.Prod
import iit.dsl.maxdsl.maximaDsl.Sine
import iit.dsl.maxdsl.maximaDsl.Sum
import iit.dsl.maxdsl.maximaDsl.VarLiteral
import iit.dsl.maxdsl.maximaDsl.TermLiteral
import iit.dsl.maxdsl.maximaDsl.Expression
import java.util.List
import iit.dsl.maxdsl.maximaDsl.Model

class Common {
	def dispatch CharSequence asCharSequence(Sum expr) '''
        «asCharSequence(expr.left)» «expr.sign» «asCharSequence(expr.right)»'''
    def dispatch CharSequence asCharSequence(Prod expr) '''
        «asCharSequence(expr.left)» * «asCharSequence(expr.right)»'''

    def dispatch CharSequence asCharSequence(Sine expr) '''
        «IF expr.minus»-«ENDIF»sin(«asCharSequence(expr.arg)»)'''

    def dispatch CharSequence asCharSequence(Cosine expr) '''
        «IF expr.minus»-«ENDIF»cos(«asCharSequence(expr.arg)»)'''

    def dispatch CharSequence asCharSequence(TermLiteral term) '''
        «term.textvalue»'''

    def dispatch CharSequence textvalue(FloatLiteral f) '''
        «IF f.minus»-«ENDIF»«f.value»'''
    def dispatch CharSequence textvalue(VarLiteral v) '''
        «IF v.minus»-«ENDIF»«v.value.name»'''


    def dispatch boolean isEqual(Expression expr1, Expression expr2) {
        return false;
    }
    def dispatch boolean isEqual(Sum expr1, Sum expr2) {
        return isEqual(expr1.left, expr2.left) && expr1.sign.equals(expr2.sign) && isEqual(expr1.right, expr2.right)
    }
    def dispatch boolean isEqual(Prod expr1, Prod expr2) {
        return isEqual(expr1.left, expr2.left) && isEqual(expr1.right, expr2.right)
    }
    def dispatch boolean isEqual(Sine expr1, Sine expr2) {
        // we do not want to consider that 'sin(...)' is different from '-sin(...)'
        //  thuse ignore the 'minus' attribute
        return isEqual(expr1.arg, expr2.arg)
    }
    def dispatch boolean isEqual(Cosine expr1, Cosine expr2) {
        return isEqual(expr1.arg, expr2.arg)
    }
    def dispatch boolean isEqual(FloatLiteral f1, FloatLiteral f2) {
        return f1.minus == f2.minus && f1.value == f2.value
    }
    def dispatch boolean isEqual(VarLiteral v1, VarLiteral v2) {
        return v1.minus == v2.minus && v1.value.name.equals(v2.value.name)
    }

    def dispatch void addTrigonometricFunctions(Sum expr, List<Sine> sines, List<Cosine> cosines) {
        addTrigonometricFunctions(expr.left, sines, cosines)
        addTrigonometricFunctions(expr.right, sines, cosines)
    }
    def dispatch void addTrigonometricFunctions(Prod expr, List<Sine> sines, List<Cosine> cosines) {
        addTrigonometricFunctions(expr.left, sines, cosines)
        addTrigonometricFunctions(expr.right, sines, cosines)
    }
    def dispatch void addTrigonometricFunctions(Sine expr, List<Sine> sines, List<Cosine> cosines) {
        if( !contains(sines, expr)) {
            sines.add(expr)
        }
    }
    def dispatch void addTrigonometricFunctions(Cosine expr, List<Sine> sines, List<Cosine> cosines) {
        if( !contains(cosines, expr)) {
            cosines.add(expr)
        }
    }
    def dispatch void addTrigonometricFunctions(Expression expr, List<Sine> sines, List<Cosine> cosines) {
        //in the "anonymous" case do not do anything
    }

    def boolean contains(List<Sine> list, Sine sineExpr) {
        for(item : list) {
            if( isEqual(item, sineExpr)) return true
        }
        return false
    }
    def boolean contains(List<Cosine> list, Cosine cosineExpr) {
        for(item : list) {
            if( isEqual(item, cosineExpr)) return true
        }
        return false
    }

    /**
     * Fills two lists with the sine and cosine expressions contained in the given model, without
     * duplicates.
     * @param model the Maxima DSL model object
     * @param sines the list that will be filled with expressions in the form sin(...)
     * @param cosines the list that will be filled with expressions in the form cos(...)
     */
    def public void getTrigonometricExpressions(Model model, List<Sine> sines, List<Cosine> cosines) {
        for(e : model.expressions) {
            addTrigonometricFunctions(e, sines, cosines)
        }
    }
}